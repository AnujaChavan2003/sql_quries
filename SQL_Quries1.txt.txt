"drop table dbms.employee ;

create table employee
(
    id int not null ,
    name varchar(50) default 'TempEmployee' ,
    department_name varchar(100),
    salary varchar(100),
    commission int, 
    check (department_name ='SALES')
);

alter table dbms.employee modify name varchar(50) not null ;

insert into dbms.employee(id,department_name,salary,commission)
values(1,'HR',2200,10) ;

truncate dbms.employee ;

select * from dbms.employee ;"